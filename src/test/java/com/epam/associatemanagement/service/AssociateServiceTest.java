package com.epam.associatemanagement.service;

import com.epam.associatemanagement.dao.AssociateRepository;
import com.epam.associatemanagement.dao.BatchRepository;
import com.epam.associatemanagement.dto.AssociateDTO;
import com.epam.associatemanagement.dto.BatchDTO;
import com.epam.associatemanagement.helper.AssociateException;
import com.epam.associatemanagement.model.Associate;
import com.epam.associatemanagement.model.Batch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
class AssociateServiceTest {

    @Mock
    AssociateRepository associateRepository;

    @Mock
    BatchRepository batchRepository;
    @InjectMocks
    AssociateServiceImpl associateServiceImpl;

    static AssociateDTO associateDTO;
    static BatchDTO batchDTO;
    static Batch batch;
    static Associate associate;
    @BeforeAll
    static void setUp() {
        batchDTO = new BatchDTO(1l,"Java-2023","Java",new Date(),new Date());
        associateDTO = new AssociateDTO(1l,"sasi","shasank@gmail.com","M","VU","Active",batchDTO);

        batch = new Batch(11l,"Java-2023","Java",new Date(),new Date());
        associate = new Associate(1l,"sasi","shasank@gmail.com","M","VU","Active",batch);
    }

    @Test
    void insertAssociateTest() {
        Mockito.when(batchRepository.save(batch)).thenReturn(batch);
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);
        assertEquals(associateDTO,associateServiceImpl.insertAssociate(associateDTO));

    }

    @Test
    void getAssociateByGenderExceptionTest() {
        Mockito.when(associateRepository.findByGender("M")).thenReturn(Optional.of(new ArrayList<>()));
        assertThrows(AssociateException.class,() -> associateServiceImpl.associateByGenders("M"));
    }

    @Test
    void getAssociateByGenderTest() {
        List<Associate> associates = new ArrayList<>(Arrays.asList(associate));
        Mockito.when(associateRepository.findByGender("M")).thenReturn(Optional.of(associates));
        associateServiceImpl.associateByGenders("M");
    }

    @Test
    void updateAssociateTest() {
        Mockito.when(batchRepository.save(batch)).thenReturn(batch);
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);

        assertEquals(associateDTO,associateServiceImpl.updateAssociate(associateDTO));
    }

    @Test
    void deleteAssociateTest() {
        Mockito.doNothing().when(associateRepository).deleteById(1l);
        associateServiceImpl.deleteAssociate(1l);
    }



}
