package com.epam.associatemanagement.restcontroller;

import com.epam.associatemanagement.dao.AssociateRepository;
import com.epam.associatemanagement.dao.BatchRepository;
import com.epam.associatemanagement.dto.AssociateDTO;
import com.epam.associatemanagement.dto.BatchDTO;
import com.epam.associatemanagement.helper.AssociateException;
import com.epam.associatemanagement.model.Associate;
import com.epam.associatemanagement.model.Batch;
import com.epam.associatemanagement.service.AssociateServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@MockitoSettings(strictness = Strictness.LENIENT)
@WebMvcTest
class AssociateControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    AssociateServiceImpl associateServiceImpl;

    @MockBean
    BatchRepository batchRepository;


    @MockBean
    AssociateRepository associateRepository;

    static AssociateDTO associateDTO;
    static BatchDTO batchDTO;
    static Batch batch;
    static Associate associate;
    @BeforeAll
    static void setUp() {
        batchDTO = new BatchDTO(11l,"Java-2023","Java",new Date(),new Date());
        associateDTO = new AssociateDTO(1l,"sasi","shasank@gmail.com","M","VU","Active",batchDTO);

        batch = new Batch(11l,"Java-2023","Java",new Date(),new Date());
        associate = new Associate(1l,"sasi","shasank@gmail.com","M","VU","Active",batch);
    }
    @Test
     void addAssociateTest() throws Exception {
        Mockito.when(associateServiceImpl.insertAssociate(any(AssociateDTO.class))).thenReturn(associateDTO);
        Mockito.when(batchRepository.save(batch)).thenReturn(batch);
        Mockito.when(associateRepository.save(associate)).thenReturn(associate);

        mockMvc.perform(post("/rd/associates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associateDTO)))
                .andExpect(status().isCreated());
    }

    @Test
    void getAssociatesTest() throws Exception {
        Mockito.when(associateServiceImpl.associateByGenders("M")).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/rd/associates/M"))
                .andExpect(status().isOk());
    }

    @Test
    void updateAssociatesTest() throws Exception {
        Mockito.when(associateServiceImpl.updateAssociate(associateDTO)).thenReturn(associateDTO);

        mockMvc.perform(put("/rd/associates")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(associateDTO)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteAssociatesTest() throws Exception {
        Mockito.doNothing().when(associateServiceImpl).deleteAssociate(1l);

        mockMvc.perform(delete("/rd/associates/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    void getAssociatesRuntimeExceptionTest() throws Exception {
        Mockito.when(associateServiceImpl.associateByGenders("M")).thenThrow(RuntimeException.class);

        mockMvc.perform(get("/rd/associates/M"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void getAssociatesExceptionTest() throws Exception {
        Mockito.when(associateServiceImpl.associateByGenders("M")).thenThrow(AssociateException.class);

        mockMvc.perform(get("/rd/associates/M"))
                .andExpect(status().isBadRequest());
    }

}
