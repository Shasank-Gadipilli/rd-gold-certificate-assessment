package com.epam.associatemanagement.service;

import com.epam.associatemanagement.dto.AssociateDTO;
import com.epam.associatemanagement.helper.AssociateException;

import java.util.List;

public interface AssociateService {

    AssociateDTO insertAssociate(AssociateDTO associateDTO);
    List<AssociateDTO> associateByGenders(String gender) throws AssociateException;
    AssociateDTO updateAssociate(AssociateDTO associateDTO);
    void deleteAssociate(Long id);
}
