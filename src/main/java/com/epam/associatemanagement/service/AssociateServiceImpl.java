package com.epam.associatemanagement.service;

import com.epam.associatemanagement.dao.AssociateRepository;
import com.epam.associatemanagement.dao.BatchRepository;
import com.epam.associatemanagement.dto.AssociateDTO;
import com.epam.associatemanagement.dto.ValueMapper;
import com.epam.associatemanagement.helper.AssociateException;
import com.epam.associatemanagement.model.Associate;
import com.epam.associatemanagement.model.Batch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AssociateServiceImpl implements AssociateService{

    private final AssociateRepository associateRepository;
    private final BatchRepository batchRepository;
    private final ValueMapper valueMapper;

    public AssociateServiceImpl(AssociateRepository associateRepository, BatchRepository batchRepository, ValueMapper valueMapper) {
        this.associateRepository = associateRepository;
        this.batchRepository = batchRepository;
        this.valueMapper = valueMapper;
    }

    @Override
    public AssociateDTO insertAssociate(AssociateDTO associateDTO) {
        log.info("insert associate method invoked {}",associateDTO);
        Batch batch = batchRepository.save(ValueMapper.convertBatchDtoToEntity(associateDTO.getBatchDTO()));
        Associate associate = ValueMapper.convertDTOToEntity(associateDTO);
        associate.setBatch(batch);
        associateRepository.save(associate);
        return associateDTO;
    }

    @Override
    public List<AssociateDTO> associateByGenders(String gender) throws AssociateException {
        log.info("associate by gender method invoked {}",gender);
        Optional<List<Associate>> associates = associateRepository.findByGender(gender);
        if(associates.get().isEmpty()) {
            throw new AssociateException("No Records found with respective Id", HttpStatus.OK);
        }

        return associates.get().stream()
                    .map(ValueMapper::convertEntityDTO)
                    .toList();
    }

    @Override
    public AssociateDTO updateAssociate(AssociateDTO associateDTO) {
        log.info("update associate method invoked {}",associateDTO);
        return insertAssociate(associateDTO);
    }

    @Override
    public void deleteAssociate(Long id) {
        log.info("delete associate method invoked {}",id);
        associateRepository.deleteById(id);
    }
}
