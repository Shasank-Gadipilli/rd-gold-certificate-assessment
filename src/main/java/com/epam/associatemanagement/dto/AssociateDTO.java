package com.epam.associatemanagement.dto;

import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;

public class AssociateDTO {

    @Id
    private Long id;
    @NotBlank(message = "name field can't be empty.")
    private String name;
    @NotBlank(message = "email field can't be empty.")
    private String email;
    @NotBlank(message = "email field can't be empty.")
    private String gender;
    @NotBlank(message = "college field can't be empty.")
    private String college;
    @NotBlank(message = "status field can't be empty.")
    private String status;
    @NotBlank(message = "batch field can't be empty.")
    private BatchDTO batchDTO;

    public AssociateDTO() {
    }



    public AssociateDTO(Long id, String name, String email, String gender, String college, String status, BatchDTO batch) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.college = college;
        this.status = status;
        this.batchDTO = batch;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BatchDTO getBatchDTO() {
        return batchDTO;
    }

    public void setBatchDTO(BatchDTO batchDTO) {
        this.batchDTO = batchDTO;
    }

    @Override
    public String toString() {
        return "AssociateDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", college='" + college + '\'' +
                ", status='" + status + '\'' +
                ", batchDTO=" + batchDTO +
                '}';
    }
}
