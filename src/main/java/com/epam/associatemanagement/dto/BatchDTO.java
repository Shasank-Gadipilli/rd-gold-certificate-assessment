package com.epam.associatemanagement.dto;

import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;

import java.util.Date;

public class BatchDTO {

    @Id
    private Long id;
    @NotBlank(message = "name field can't be empty.")
    private String name;
    @NotBlank(message = "practice field can't be empty.")
    private String practice;
    @NotBlank(message = "startDate field can't be empty.")
    private Date startDate;
    @NotBlank(message = "endDate field can't be empty.")
    private Date endDate;

    public BatchDTO(Long id, String name, String practice, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.practice = practice;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public BatchDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPractice() {
        return practice;
    }

    public void setPractice(String practice) {
        this.practice = practice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
