package com.epam.associatemanagement.dto;


import com.epam.associatemanagement.model.Associate;
import com.epam.associatemanagement.model.Batch;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public class ValueMapper {

    private ValueMapper(){}

    public static Associate convertDTOToEntity(AssociateDTO associateDTO) {
        log.info("convert Associate DTO to Entity : "+associateDTO);
        Associate associate = new Associate();
        if(associateDTO.getId() != null) {
            associate.setId(associateDTO.getId());
        }
        associate.setName(associateDTO.getName());
        associate.setGender(associateDTO.getGender());
        associate.setEmail(associateDTO.getEmail());
        associate.setCollege(associateDTO.getCollege());
        associate.setBatch(convertBatchDtoToEntity(associateDTO.getBatchDTO()));
        associate.setStatus(associateDTO.getStatus());
        return associate;
    }
    public static AssociateDTO convertEntityDTO(Associate associate) {
        log.info("convert Associate Entity to DTO: "+associate);
        AssociateDTO associateDTO = new AssociateDTO();
        associateDTO.setId(associate.getId());
        associateDTO.setName(associate.getName());
        associateDTO.setGender(associate.getGender());
        associateDTO.setEmail(associate.getEmail());
        associateDTO.setCollege(associate.getCollege());
        associateDTO.setBatchDTO(convertEntityToBatchDTO(associate.getBatch()));
        associateDTO.setStatus(associate.getStatus());
        return associateDTO;
    }

    public static Batch convertBatchDtoToEntity(BatchDTO batchDTO) {
        log.info("convert Batch DTO to Entity : "+batchDTO);
        Batch batch = new Batch();
        if(Optional.ofNullable(batchDTO.getId()).isPresent()) {
            batch.setId(batchDTO.getId());
        }
        batch.setName(batchDTO.getName());
        batch.setPractice(batchDTO.getPractice());
        batch.setStartDate(batchDTO.getStartDate());
        batch.setEndDate(batchDTO.getEndDate());
        return batch;
    }

    public static BatchDTO convertEntityToBatchDTO(Batch batch) {
        log.info("convert Batch Entity to DTO : "+batch);
        BatchDTO batchDTO = new BatchDTO();
        if(batch.getId()!=null) {
            batchDTO.setId(batch.getId());
        }
        batchDTO.setName(batch.getName());
        batchDTO.setPractice(batch.getPractice());
        batchDTO.setStartDate(batch.getStartDate());
        batchDTO.setEndDate(batch.getEndDate());
        return batchDTO;
    }
}
