package com.epam.associatemanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@OpenAPIDefinition(info = @Info(title = "Associate-Management",version = "1.0"))
@SpringBootApplication
public class AssociateManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssociateManagementApplication.class, args);
    }


    @Bean
    ModelMapper getModelMapper() {
        return new ModelMapper();
    }
}
