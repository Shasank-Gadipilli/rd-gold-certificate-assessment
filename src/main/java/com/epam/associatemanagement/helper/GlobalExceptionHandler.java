package com.epam.associatemanagement.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<ExceptionResponse> handleEmptyFields(MethodArgumentNotValidException exception, WebRequest request) {
        log.info("handle exception handler invoked.");
        StringBuilder message=new StringBuilder();
        exception.getAllErrors().forEach(error ->
                message.append(error.getDefaultMessage()).append("\n")
        );
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),message.toString(), HttpStatus.BAD_REQUEST.name(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AssociateException.class})
    public ResponseEntity<ExceptionResponse> handleAssociateException(AssociateException exception, WebRequest request) {
        log.info("handle associate exception handler invoked.");
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(), exception.getMessage(), HttpStatus.BAD_REQUEST.name(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<ExceptionResponse> handleRuntimeException(RuntimeException exception, WebRequest request) {
        log.info("handle runtime exception handler invoked.");
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(), exception.getMessage(), HttpStatus.BAD_REQUEST.name(), request.getDescription(false));
        return new ResponseEntity<>(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
