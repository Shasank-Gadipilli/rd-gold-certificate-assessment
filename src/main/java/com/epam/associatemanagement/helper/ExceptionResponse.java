package com.epam.associatemanagement.helper;

public class ExceptionResponse {
    private String timestamp;
    private String status;
    private String error;
    private String path;

    public ExceptionResponse(String timestamp, String status, String error, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.path = path;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getPath() {
        return path;
    }
}
