package com.epam.associatemanagement.helper;

import org.springframework.http.HttpStatus;

public class AssociateException extends RuntimeException {

    private HttpStatus status;
    public AssociateException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
}
