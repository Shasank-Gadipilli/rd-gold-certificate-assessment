package com.epam.associatemanagement.restcontroller;

import com.epam.associatemanagement.dto.AssociateDTO;
import com.epam.associatemanagement.helper.AssociateException;
import com.epam.associatemanagement.service.AssociateService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
public class AssociateController {

    private final AssociateService associateServiceImpl;

    public AssociateController(AssociateService associateServiceImpl) {
        this.associateServiceImpl = associateServiceImpl;
    }

    @PostMapping("/rd/associates")
    public ResponseEntity<AssociateDTO> addAssociate(@Valid @RequestBody AssociateDTO associateDTO) {
        log.info("Add associate Method Handler invoked with {}",associateDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(associateServiceImpl.insertAssociate(associateDTO));
    }

    @GetMapping("/rd/associates/{gender}")
    public ResponseEntity<List<AssociateDTO>> retrieveAssociate(@PathVariable @Valid @NotBlank(message = "gender can't be empty.") String gender) throws AssociateException {
        log.info("retrieve associate Method Handler invoked with {}",gender);
        return ResponseEntity.status(HttpStatus.OK).body(associateServiceImpl.associateByGenders(gender));
    }

    @PutMapping("/rd/associates")
    public ResponseEntity<AssociateDTO> updateAssociate(@Valid @RequestBody  AssociateDTO associateDTO) {
        log.info("update associate Method Handler invoked with {}",associateDTO);
        return ResponseEntity.status(HttpStatus.OK).body(associateServiceImpl.updateAssociate(associateDTO));
    }

    @DeleteMapping("/rd/associates/{id}")
    public ResponseEntity<Void> deleteAssociate(@PathVariable Long id) {
        log.info("delete associate Method Handler invoked with {}",id);
        associateServiceImpl.deleteAssociate(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
