package com.epam.associatemanagement.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity(name = "batches")
public class Batch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String practice;
    private Date startDate;
    private Date endDate;

    @OneToMany(mappedBy = "batch",cascade = CascadeType.ALL)
    @JsonBackReference
    private List<Associate> associate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPractice() {
        return practice;
    }

    public void setPractice(String practice) {
        this.practice = practice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Batch(Long id, String name, String practice, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.practice = practice;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Batch() {
    }
}
