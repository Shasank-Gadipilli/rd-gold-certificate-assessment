package com.epam.associatemanagement.model;

import jakarta.persistence.*;

@Entity(name = "associates")
public class Associate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String gender;
    private String college;
    private String status;

    @ManyToOne
    @JoinColumn(name = "batch_id")
    private Batch batch;


    public Associate() {
    }

    public Associate(Long id, String name, String email, String gender, String college, String status, Batch batch) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.college = college;
        this.status = status;
        this.batch = batch;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }
}
